# Flectra Community / vertical-rental

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[rental_pricelist_interval](rental_pricelist_interval/) | 2.0.1.0.0| Enables the user to define different rental prices time uom (Month, Day and Hour).
[rental_pricelist](rental_pricelist/) | 2.0.1.0.1| Enables the user to define different rental prices with time uom (Month, Day and Hour).
[rental_offday](rental_offday/) | 2.0.1.0.0| Manage off-days in rentals on daily basis
[rental_product_pack](rental_product_pack/) | 2.0.1.0.1| Manage rentals with product packs
[rental_base](rental_base/) | 2.0.1.0.1| Manage Rental of Products


