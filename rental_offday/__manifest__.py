# Part of rental-vertical See LICENSE file for full copyright and licensing details.

{
    "name": "Rental Off-Day",
    "summary": "Manage off-days in rentals on daily basis",
    "version": "2.0.1.0.0",
    "category": "Rental",
    "author": "elego Software Solutions GmbH, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/vertical-rental",
    "depends": [
        "rental_pricelist",
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/sale_view.xml",
    ],
    "demo": [],
    "qweb": [],
    "auto_install": False,
    "application": False,
    "installable": True,
    "license": "AGPL-3",
}
